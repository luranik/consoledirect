#include "OpenNiInterface.h"

using namespace openni;
struct OpenniInterface {
public:

static bool HandleStatus (Status theStatus)
{
    if (theStatus == STATUS_OK) {
        return true;
    }
    std::cout << "ERROR: #" << theStatus << OpenNI::getExtendedError () << std::endl;
    
    ConsoleInterface::UserInputHandle ();

    return false;
}

//TODO split to a few functions?
static bool DeviceFormattedInfo()
{
    Array<openni::DeviceInfo> aListOfDevices;
    OpenNI::enumerateDevices (&aListOfDevices);
    int aNumOfDevices = aListOfDevices.getSize();
    if (aNumOfDevices > 0) {
        for (int i = 0; i < aNumOfDevices; ++i) {
            openni::DeviceInfo aDevice = aListOfDevices[i];
            std::cout << aDevice.getVendor() << " " << aDevice.getName()
                << " VID: " << aDevice.getUsbVendorId()
                << " PID: " << aDevice.getUsbProductId()
                << " is connected at " << aDevice.getUri() << std::endl;
        }
    } else {
        std::cout << "No devices connected.";
    }
    return true;
}

static void KinectUri (std::string& theKinectUri)
{
    Array<openni::DeviceInfo> aListOfDevices;
    OpenNI::enumerateDevices (&aListOfDevices);
    int aDeviceNum = aListOfDevices.getSize ();
    if (aDeviceNum > 0) {
        for (int i = 0; i < aDeviceNum; ++i) {
            std::string aDeviceName (aListOfDevices[i].getName());
            if (aDeviceName == "Kinect") {
                theKinectUri = std::string (aListOfDevices[i].getUri());
            }
        }
    }
}

static void VideoModeFormattedInfo (const VideoMode &theVM)
{
    std::cout << theVM.getResolutionX() << " x "
            << theVM.getResolutionY() << " at "
            << theVM.getFps() << " fps with "
            << theVM.getPixelFormat() << " pixel format.\n";
}

};
