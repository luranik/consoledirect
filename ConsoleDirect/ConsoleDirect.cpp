// ConsoleDirect.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

//temporary constants
const int myWidght = 640;
const int myHeight = 480;


template <class Interface>
inline void SafeRelease (Interface *&thePInterfaceToRelease) {
    if (thePInterfaceToRelease != NULL) {
        (thePInterfaceToRelease)->Release();
        (thePInterfaceToRelease) = NULL;
    }
}

struct ImageRenderer {

public:
    ImageRenderer() :
        myHWindow (0),
        myStreamHeight (0),
        myStreamWidth (0),
        myStreamStride (0),
        myPD2DFactory (NULL),
        myPRenderTarget (NULL),
        myPBitmap (0) 
        {}

    ~ImageRenderer() {
        DiscardResources();
        SafeRelease (myPD2DFactory);
    }

HRESULT Initialize (HWND            theHWindow, 
                    ID2D1Factory*   thePD2dFactory, 
                    unsigned int    theStreamHeight,
                    unsigned int    theStreamWidth,
                    long            theStreamStride) {

    if (thePD2dFactory == NULL) {
        return E_INVALIDARG;
    }

    myHWindow = theHWindow;

    myPD2DFactory = thePD2dFactory;
    myPD2DFactory->AddRef();

    myStreamHeight = theStreamHeight;
    myStreamWidth = theStreamWidth;
    myStreamStride = theStreamStride;

    return S_OK;
}

HRESULT Draw (unsigned char* thePImage, unsigned long theImageSize) {

    if (theImageSize < ((myStreamHeight - 1) * myStreamStride) + (myStreamWidth * 4)) {
        return E_INVALIDARG;
    }

    HRESULT aRes = EnsureResources();

    if (FAILED (aRes)) {
        return aRes;
    }

    aRes = myPBitmap->CopyFromMemory (NULL, thePImage, myStreamStride);

    if (FAILED (aRes)) {
        return aRes;
    }

    myPRenderTarget->BeginDraw();
    myPRenderTarget->DrawBitmap (myPBitmap);

    aRes = myPRenderTarget->EndDraw();

    if (aRes == D2DERR_RECREATE_TARGET) {
        aRes = S_OK;
        DiscardResources();
    }

}



private:
    HWND                    myHWindow;

    unsigned int            myStreamHeight;
    unsigned int            myStreamWidth;
    long                    myStreamStride;

    ID2D1Factory*           myPD2DFactory;
    ID2D1HwndRenderTarget*  myPRenderTarget;
    ID2D1Bitmap*            myPBitmap;


HRESULT EnsureResources() {
    HRESULT aRes = S_OK;

    if (myPRenderTarget == NULL) {
        D2D1_SIZE_U aStreamSize = D2D1::SizeU (myStreamHeight, myStreamWidth);

        D2D1_RENDER_TARGET_PROPERTIES aRTProps = D2D1::RenderTargetProperties();
        aRTProps.pixelFormat = D2D1::PixelFormat (DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE);
        aRTProps.usage = D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE;

        aRes = myPD2DFactory->CreateHwndRenderTarget (
            aRTProps,
            D2D1::HwndRenderTargetProperties (myHWindow, aStreamSize),
            &myPRenderTarget);

        if (FAILED (aRes)) {
            return aRes;
        }

        aRes = myPRenderTarget->CreateBitmap (
            aStreamSize,
            D2D1::BitmapProperties (aRTProps.pixelFormat),
            &myPBitmap);

        if (FAILED (aRes)) {
            SafeRelease (myPRenderTarget);
            return aRes;
        }

    }

    return aRes;
}

void DiscardResources() {
    SafeRelease (myPRenderTarget);
    SafeRelease (myPBitmap);
}
};



struct DirectWindow {
public:
    DirectWindow(): 
    myPD2DFactory (NULL),
    myImageRenderer (NULL),
    myNextColorFrameEvent (INVALID_HANDLE_VALUE),
    myPColorStream (INVALID_HANDLE_VALUE)
    //to kinect pointer
    {}
    ~DirectWindow();

int Run (HINSTANCE theHInstance, int theCmdShow) {
    HWND aWindow = CreateDialogParamW(
        theHInstance,
        MAKEINTRESOURCE(110),
        NULL /*aWindowParent*/,
        (DLGPROC)MessageRouter,
        reinterpret_cast<LPARAM> (this));
}

static LRESULT CALLBACK MessageRouter (HWND theWindow,
                                       UINT theMessage,
                                       WPARAM theWParam,
                                       LPARAM theLParam) {
    DirectWindow* aPThis = nullptr;
    if (theMessage == WM_INITDIALOG) {
        aPThis = reinterpret_cast <DirectWindow*> (theLParam);
        SetWindowLongPtr (theWindow, GWLP_USERDATA, reinterpret_cast <LONG_PTR> (aPThis));
    } else {
        aPThis = reinterpret_cast <DirectWindow*> (GetWindowLongPtr (theWindow, GWLP_USERDATA));
    }

    //!!!

}

LRESULT CALLBACK DialogProcess (HWND theWindow, UINT theMessage, WPARAM theWParam, LPARAM theLParam) {

    switch (theMessage) {
        case WM_INITDIALOG: {
            myWindow = theWindow;
            D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &myPD2DFactory);
            myImageRenderer = new ImageRenderer();

            HRESULT aRes = myImageRenderer->Initialize(
                GetDlgItem(myWindow, 1003 /*video view*/),
                myPD2DFactory,
                myWidght,
                myHeight,
                myWidght * sizeof(long));

            if (FAILED (aRes)) {
                std::cout << "Failed to initialize Direct2d/n";
            }

            //CreateFirstConnection - kinect connection
        }
    }
}

HRESULT KinectConnection ()
{
    using namespace openni;
    Status aStatus= STATUS_OK;
    aStatus = OpenNI::initialize ();
}

private:
    HWND                myWindow;
    //to kinect pointer;
    ImageRenderer*      myImageRenderer;
    ID2D1Factory*       myPD2DFactory;

    HANDLE              myPColorStream;
    HANDLE              myNextColorFrameEvent;


};


int main()
{

    ID2D1Factory* aD2DFactory = NULL;
    HRESULT aHR = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &aD2DFactory);

    return 0;
}

