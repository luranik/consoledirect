#include "stdafx.h"

#pragma once


using namespace openni;
struct OpenniInterface {
public:

    static bool HandleStatus (Status theStatus);

    //TODO split to a few functions?
    static bool DeviceFormattedInfo ();

    static void KinectUri (std::string& theKinectUri);

    static void VideoModeFormattedInfo (const VideoMode &theVM);

};