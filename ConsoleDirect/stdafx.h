// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include "ConsoleInterface.h"

#include <stdio.h>
#include <tchar.h>
#include <malloc.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>
#include <iostream>

#include <OpenNI.h>




// TODO: reference additional headers your program requires here
